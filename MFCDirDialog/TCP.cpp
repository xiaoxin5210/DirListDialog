// TCP.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <iostream>
#include <WinSock2.h>
#include <vector>

using namespace std;

SOCKET socketClent = 0;
SOCKADDR_IN addrSrv = { 0 };
IFileOpenDialog* pfd = nullptr;
HRESULT hr;
//接收数据线程
DWORD WINAPI Thread1( LPVOID lpPara );

int _tmain(int argc, _TCHAR* argv[])
{
	//判断输入参数的是否大于2 防止直接点击启动
	if (argc < 2)
	{
		cout << argc << endl;
		return 0;
	}

	//判断输入参数2是否为123相当于密码
	if (0 != strcmp("123", argv[1]))
	{
		cout << argv[1] << endl;
		return 0;
	}

	WORD wVersionRequested = 0;
	WSADATA wsaData = { 0 };
	int err = 0;

	wVersionRequested = MAKEWORD( 1, 1 );

	//设置套接字版本号1.1
	err = WSAStartup( wVersionRequested, &wsaData );
	if( 0 != err )
	{
		return 0;
	}
	if( 1 != LOBYTE( wsaData.wVersion ) || 1 != HIBYTE( wsaData.wVersion ) )
	{
		return 0;
	}

	printf( "\t客户端：\n");
	socketClent = socket( AF_INET, SOCK_STREAM, 0 );

	addrSrv.sin_addr.S_un.S_addr = inet_addr( "127.0.0.1" );
	addrSrv.sin_family = AF_INET;
	addrSrv.sin_port = htons(11020);

	//连接服务端
	int iRet = connect( socketClent, ( SOCKADDR* )&addrSrv, sizeof( SOCKADDR ) );
	if( SOCKET_ERROR == iRet )
	{
		cout << "连接失败" << endl;
		system("pause");
		return 0;
	}
	
	HANDLE hThread = CreateThread( NULL, 0, Thread1, NULL, 0, NULL );

	if(NULL != hThread)
	{
		CloseHandle( hThread );
		hThread = NULL;
	}
	

	char recvBuf[1024] = { 0 };

	//客户端可以主动给服务器发数据
	while( true )
	{
		cin >> recvBuf;
		send( socketClent, recvBuf, strlen( recvBuf ) + 1, 0 );
	}

	closesocket( socketClent );
	WSACleanup();

	return 0;
}

std::vector<CString>Get_path_dlg(HWND hwndOwner)
{
	std::vector<CString>ans;
	CWnd* w = new CWnd();
	w->FromHandle(hwndOwner);
	CoInitialize(NULL);
	DWORD dwFlags;
	if (pfd != nullptr)
	{
		cout << "11111" << endl;
		return ans;
	}
	hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pfd));
	hr = pfd->GetOptions(&dwFlags);
	hr = pfd->SetOptions(dwFlags | FOS_ALLOWMULTISELECT | FOS_PICKFOLDERS);//support multiselect folder;
	//hr = pfd->Show(hwndOwner);//show open file dlg;
	pfd->Close(hr);
	IShellItemArray *pShellItem = nullptr;

	if (hr == S_OK)
	{
		hr = pfd->GetResults(&pShellItem);
		pfd = nullptr;
		DWORD dwNumItems = 0;
		pShellItem->GetCount(&dwNumItems);

		for (int i = 0; i < dwNumItems; ++i)
		{
			IShellItem *pItem = nullptr;
			hr = pShellItem->GetItemAt(i, &pItem);
			LPWSTR lpSelect = nullptr;
			pItem->GetDisplayName(SIGDN_DESKTOPABSOLUTEPARSING, &lpSelect);
			CString strSelect(lpSelect);//file path
			ans.push_back(strSelect);//将获得的路径存入Vector容器中
			CoTaskMemFree(lpSelect);//free memory;
		}
	}
	else
	{
		pfd = nullptr;
	}
	CoUninitialize();//释放COM套件

	return ans;
}

void SendInfo(char* pBuf)
{
	if(0 == strcmp(pBuf, "1"))
	{
		HWND cmd=GetConsoleWindow();
		//HWND hWnd = ::GetSafeHwnd();
		vector<CString> vecFile;
		vecFile = Get_path_dlg(cmd);
		CString dirList;
		for (int i = 0; i < vecFile.size(); i++)
		{
			dirList.Append(vecFile[i]);
			dirList.Append("|");
		}
		send(socketClent, dirList.GetBuffer(0), dirList.GetLength() + 1, 0);
	}

}

//接收服务端数据
DWORD WINAPI Thread1( LPVOID lpPara )
{
	char recvBuf[1024] = { 0 };
	int iLen = 0;
	while( TRUE )
	{
		char cRecv;
		int iRecv = recv( socketClent, recvBuf, 1024, 0 ); //阻塞接收数据
		if( -1 == iRecv )
		{
			cout << "断开连接" <<endl;
			continue;
			//break;
		}

		SendInfo(recvBuf);
		memset(recvBuf, 0, 1024);
		/*recvBuf[iLen] = cRecv;

		if('\0' == cRecv)
		{
		SendInfo(recvBuf);
		iLen = 0;
		memset(recvBuf, 0, 1024);
		}
		else
		{
		iLen++;
		}*/
	}

	return 0;
}
